## Welcome to CICD Workshop


### docker build -t cicddemoapp .

```
Balas-MacBook-Air:cicddemo std-user01$ docker build -t cicddemoapp .
Sending build context to Docker daemon  84.99kB
Step 1/8 : FROM python:3.7-alpine
3.7-alpine: Pulling from library/python
cd784148e348: Pull complete
a5ca736b15eb: Pull complete
f320f547ff02: Pull complete
2edd8ff8cb8f: Pull complete
81edffd1db2b: Pull complete
Digest: sha256:698816aee98b943878158ba950c83773c41ec98ff7117e5c9290e070957de246
Status: Downloaded newer image for python:3.7-alpine
 ---> 70fb5fb468a7
Step 2/8 : LABEL name=bala@nus.edu.sg
 ---> Running in 7316e397eb2d
Removing intermediate container 7316e397eb2d
 ---> db0c64dec30c
Step 3/8 : WORKDIR /app
 ---> Running in ff3b21918e2e
Removing intermediate container ff3b21918e2e
 ---> 33d8507a778d
Step 4/8 : COPY requirements.txt /app/requirements.txt
 ---> 6594a55fabea
Step 5/8 : RUN pip install -r requirements.txt
 ---> Running in d10e1cc64f48
Collecting flask (from -r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/7f/e7/08578774ed4536d3242b14dacb4696386634607af824ea997202cd0edb4b/Flask-1.0.2-py2.py3-none-any.whl (91kB)
Collecting gunicorn (from -r requirements.txt (line 2))
  Downloading https://files.pythonhosted.org/packages/8c/da/b8dd8deb741bff556db53902d4706774c8e1e67265f69528c14c003644e6/gunicorn-19.9.0-py2.py3-none-any.whl (112kB)
Collecting click>=5.1 (from flask->-r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/fa/37/45185cb5abbc30d7257104c434fe0b07e5a195a6847506c074527aa599ec/Click-7.0-py2.py3-none-any.whl (81kB)
Collecting itsdangerous>=0.24 (from flask->-r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/76/ae/44b03b253d6fade317f32c24d100b3b35c2239807046a4c953c7b89fa49e/itsdangerous-1.1.0-py2.py3-none-any.whl
Collecting Werkzeug>=0.14 (from flask->-r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/20/c4/12e3e56473e52375aa29c4764e70d1b8f3efa6682bef8d0aae04fe335243/Werkzeug-0.14.1-py2.py3-none-any.whl (322kB)
Collecting Jinja2>=2.10 (from flask->-r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/7f/ff/ae64bacdfc95f27a016a7bed8e8686763ba4d277a78ca76f32659220a731/Jinja2-2.10-py2.py3-none-any.whl (126kB)
Collecting MarkupSafe>=0.23 (from Jinja2>=2.10->flask->-r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/ac/7e/1b4c2e05809a4414ebce0892fe1e32c14ace86ca7d50c70f00979ca9b3a3/MarkupSafe-1.1.0.tar.gz
Building wheels for collected packages: MarkupSafe
  Building wheel for MarkupSafe (setup.py): started
  Building wheel for MarkupSafe (setup.py): finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/81/23/64/51895ea52825dc116a55f37043f49be0939bcf603de54e5cde
Successfully built MarkupSafe
Installing collected packages: click, itsdangerous, Werkzeug, MarkupSafe, Jinja2, flask, gunicorn
Successfully installed Jinja2-2.10 MarkupSafe-1.1.0 Werkzeug-0.14.1 click-7.0 flask-1.0.2 gunicorn-19.9.0 itsdangerous-1.1.0
Removing intermediate container d10e1cc64f48
 ---> c0a9b49a89dc
Step 6/8 : COPY . /app
 ---> a2bb366c89ef
Step 7/8 : ENV PORT=5000
 ---> Running in 0ce449b82ccf
Removing intermediate container 0ce449b82ccf
 ---> 7bf1495eeab3
Step 8/8 : CMD ["gunicorn", "--bind", "0.0.0.0" ,"app:app"]
 ---> Running in e18ecdae2c28
Removing intermediate container e18ecdae2c28
 ---> 18ef7e2ebed5
Successfully built 18ef7e2ebed5
Successfully tagged cicddemoapp:latest
```


### Run the container locally

```
docker run -it -p 5000:5000 cicddemoapp
```


### Setup K8S Cluster


### Deploy the docker image manually using kubectl & deployment

```
Balas-MacBook-Air:cicddemo std-user01$ docker tag cicddemoapp balanus/cicddemoapp
Balas-MacBook-Air:cicddemo std-user01$ docker login
Authenticating with existing credentials...
Login Succeeded
Balas-MacBook-Air:cicddemo std-user01$ docker push balanus/cicddemoapp
The push refers to repository [docker.io/balanus/cicddemoapp]
177b1f7b460b: Pushed
53c2a1c00a6f: Pushed
f47e2c9b9a1f: Pushed
6f5bc0cb6592: Pushed
db7cf36c33e9: Mounted from library/python
43e8a7decf15: Mounted from library/python
7484166a246c: Mounted from library/python
1c862c0e1a30: Mounted from library/python
7bff100f35cb: Mounted from library/python
latest: digest: sha256:34699ec0cc8a86ad8fbcde47dbcf56a13a7d45b46b56ed457496ce7e49cf8696 size: 2201

kubectl apply -f manifest/deployment.yaml
kubectl apply -f manifest/service.yaml
```