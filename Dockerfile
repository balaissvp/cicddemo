FROM python:3.7-alpine
LABEL name=bala@nus.edu.sg

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

COPY . /app

ENV PORT=5000

CMD ["gunicorn", "--bind", "0.0.0.0" ,"app:app"]