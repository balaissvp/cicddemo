from flask import Flask, request, Response
import os
from datetime import datetime

app = Flask(__name__)

PORT = 5000
if os.getenv("PORT"): PORT = int(os.getenv("PORT"))

@app.route("/", methods=["GET"])
def index():
    resp = f"<h1> Current Time {str(datetime.now())} </h1>"
    return Response(resp, 200)

app.run(host='0.0.0.0', port=PORT)